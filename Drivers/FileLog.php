<?php

require_once 'vendor/autoload.php';
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class FileLog implements LoggerInterface{

    static private $instance;
    private function __construct(){}

    static public function getInstance()
    {
        if (self::$instance == null) {
            if (!self::$instance instanceof FileLog) {
                self::$instance = new FileLog();
            }
        }
   
        return self::$instance;
    }



    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function emergency($message, array $context = array()) {
        $context['loglevel'] = LogLevel::EMERGENCY;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Action must be taken immediately.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function alert($message, array $context = array()) {
        $context['loglevel'] = LogLevel::ALERT;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Critical conditions.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function critical($message, array $context = array()) {
        $context['loglevel'] = LogLevel::CRITICAL;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Runtime errors that do not require immediate action but should
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function error($message, array $context = array()) {
        $context['loglevel'] = LogLevel::ERROR;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Exceptional occurrences that are not errors.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function warning($message, array $context = array()) {
        $context['loglevel'] = LogLevel::WARNING;
        FileLog::getInstance()->interpolate($message, $context);
    }
    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function notice($message, array $context = array()) {
        $context['loglevel'] = LogLevel::NOTICE;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Interesting events.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function info($message, array $context = array()) {
        $context['loglevel'] = LogLevel::INFO;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     * @return null
     */
    public function debug($message, array $context = array()) {
        $context['loglevel'] = LogLevel::DEBUG;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return null
     */
    public function log($level, $message, array $context = array()) {
        $context['loglevel'] = LogLevel::EMERGENCY;
        FileLog::getInstance()->interpolate($message, $context);
    }
 
    /**
     * Interpolates context values into the message placeholders.
     * Taken from PSR-3's example implementation.
     */
    protected function interpolate($message, array $context = array()) {
        $date = date("Y-m-d" , time());
        $path = "./" . $date ."log.txt";
        // $path = "Drivers/" . $date ."log.txt";
  

        if (file_exists($path)) {
            $myfile = fopen($path, "a");
        } else {
            $myfile = fopen($path, "w");
        }

        $now = date("Y-m-d h:i:s" , time());

        $txt = strtoupper($context['loglevel']) . "-" . $message  .'---'. $now  ."\n ";
        fwrite($myfile, $txt);

        fclose($myfile);


        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }
 
        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }

}