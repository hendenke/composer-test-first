<?php

require_once 'Drivers/FileLog.php';
class Log {
    public static function emergency(string $message =''){
        FileLog::getInstance()->emergency( $message ,  []);
    }
    public static function alert(string $message =''){
        FileLog::getInstance()->alert( $message ,  []);
    }
    public static function critical(string $message =''){
        FileLog::getInstance()->critical( $message ,  []);
    }
    public static function error(string $message =''){
        FileLog::getInstance()->error( $message ,  []);
    }
    public static function warning(string $message =''){
        FileLog::getInstance()->warning( $message ,  []);
    }
    public static function notice(string $message =''){
        FileLog::getInstance()->notice( $message ,  []);
    }
    public static function info(string $message =''){
        FileLog::getInstance()->info( $message ,  []);
    }
    public static function debug(string $message =''){
        FileLog::getInstance()->debug( $message ,  []);
    }
}